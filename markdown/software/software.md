# Software I Use

<span class="blogdate">Originally posted Mon 08 Feb 20:25:49 2021</span>

## Vim

See [my page about Vim.](SSGPAGE(vim.md))
The best advice I can give to anyone aspiring to be productive when programming is to not use Vim.
Its level of customisability was one of the things that got me into Linux 'ricing',
a tremendous waste of time on the whole.

I also like using highly customisable file viewers that have Vim-like key bindings.
E.g. Zathura, sxiv, mpv, surf (yes, HTML is a file type).

## Emacs

The battle between Emacs and Vim users is as old as time.
Many see it as a war in which there can only be one victor,
and in modern times, it appears that it will be Vim.
I don't think it's a good idea to become too emotionally or ideologically
attached to the software or tools you use.
If there's a tool that can perform certain tasks better than the alternatives,
you should use it.

As an aside, Emacs is a lot of fun.

## Linux

After distro-hopping in 2018-2019, I settled on Arch Linux from 2019
to 2024. I appreciated the approach of adding what I wanted, rather
than removing what I didn't.

In 2024, [I switched to
Guix](SSGPAGE(../blog/24-05-03_13.27-Emacs-Desktop-Environment-EXWM-and-Guix.md)),
a transactional distro, configured and largely *implemented* in
Scheme. It even includes a service manager called shepherd that lets
you define services in Scheme, and the system configuration itself is
written in a single Scheme file. This allowed me to move almost my
entire setup into my dotfiles repository, which should make it easier
than ever to keep my configuration backed up and synchronised between
computers.

## Suckless

I use most of the major [suckless](https://suckless.org/) programs, because over time I have
individually decided that each one does the thing it does the way I want it to.

## Thunderbird

...because e-mail is something that I do need to just work.
