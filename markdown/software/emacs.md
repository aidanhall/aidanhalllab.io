# Emacs

<span class="blogdate">Originally posted 16 May 13:58:01 2022</span>

I've made a few blog posts
[comparing Emacs and Vim](
SITE_URL/blog/21-07-08_22.11.html)
and
[discussing some of my earliest experiences with Emacs](
SITE_URL/blog/21-07-21_10.19.html).
It has now been over a year since I first tried Emacs, and it has been
nearly a year since I started using it regularly.

There are a million different memes about how one defines Emacs.
My personal favourite is that it is 
[a framework to build your own text editor](
https://turbomack.github.io/posts/2016-12-29-from-vim-to-emacs-year-later.html).

Over time, however, I’ve come to realise that the most significant
quality of Emacs that appeals to me is its 40 years-worth of code
accretion.
The system has existed for so much time that there are myriad obscure
and oddly specific functions and packages to do all manner of things.

The following are a few of highly questionable usefulness that I was
nonetheless pleased to stumble across:

* `copyright.el`
* `footnote.el`
* `table.el`
* `dictionary.el`

The major drawback of having all this code haphazardly built up is
that there are often multiple incompatible ways to do the same thing.

## ‘Vanilla’ Emacs

When there are multiple competing Emacs packages or scripts that could
all meet a certain need of mine to an acceptable degree, I use a
subjective judgement of how much they feel like a natural extension of
‘vanilla’ Emacs.
I have carried this sentiment over from Vim, which doesn't have
good, standardised plugin distribution or management systems; in that
case, every additional plugin was a potential source of pain, so using
vanilla features had an obvious, concrete advantage.

In [an EmacsConf talk](https://emacsconf.org/2020/talks/39/),
Stallman describes having packages in the GNU
<abbr title="Emacs Lisp Package Archive">ELPA</abbr>
as primarily a convenience,
and that packages in ELPA are fundamentally ‘part of Emacs’.
The primary significance of all this is that packages in ELPA
(and henceforth NonGNU ELPA)
are audited to ensure they will integrate well with the vanilla Emacs
experience.
As a result, packages from ELPA have some guarantee of behaving more
in the way I would like (as opposed to ones from independent archives
such as the popular [MELPA](https://melpa.org)).

The specific case of `eglot` and `lsp-mode` comes to mind.
Both provide
<abbr title="Language Server Protocol">LSP</abbr>
support for Emacs.
The `lsp-mode` package is the larger and more ‘user-friendly’ of the
two,
but in my experience does poor job of integrating with existing
Emacs features,
notably `xref` and `completion-at-point`.
By contrast, `eglot` feels a lot more minimal and ‘Emacsy’.

Many similar sets of packages exist.
Each list is in ascending order of ‘Emacsiness’.

* `projectile` and `project`;
* `company` and `corfu`;
* `helm`, `ivy`, `vertico` and `icomplete`;
* `magit` and `vc`;
* `evil` and `viper`;
* `hydra` and `transient`.

The case of `evil` and `viper` is slightly more nuanced, since `viper`
is a ‘finished’ implementation of Vi, whereas `evil` aims specifically
to reproduce the behaviour of Vim, as I discussed in my
[Levels of Vim
Emulation](
https://argletrough.hopto.org/blog/21-10-29_23.48.html#text-editors)
blog post.

In general, I'm not saying that one should always avoid the less
‘Emacsy’ packages.
Indeed, I could most objectively describe Emacsiness as the quality of
having fewer features and being more jank.
The concrete advantages of the more ‘Emacsy’ packages will generally be
similar to those of vanilla Vim packages: being lighter weight and
either built-in or easy to add.

The notable example for weight is `vc` vs `magit`.
Although `magit` is far more feature-complete and easier to learn,
it is quite heavy, and can take a substantial number of seconds to
open on my (admittedly ancient) laptop.
It also seems to be designed for all use of it to be done via the
`magit-status` interface.
By contrast, `vc` is small, and built-in, and is reasonably quick for
basic operations.
The ability to access many of the operations you would want to perform
on a given file or the whole repository via the `vc-prefix-map`, bound
by default to <kbd>C-x</kbd> <kbd>v</kbd>,
is sufficiently distinct from the behaviour of `magit` that there is
value in using both.

The power of choice is what Emacs is all about.

Praise [Saint IGNUcius](https://stallman.org/saint.html)!
