# Equivalent Features in Emacs and Vim

<span class="blogdate">Originally posted Thu 16 Jun 13:00:49 2022</span>

<style>
table {
  border: 1px solid;
  border-collapse: collapse;
}
</style>

Both are programmable text editors which serve the same task, and so
inevitably share many equivalent features.
Here are just a few which may be less than obvious or that I find
particularly pleasing.

| Feature                  | Vim                          | Emacs                                        |
|--------------------------|------------------------------|----------------------------------------------|
| Open filename at point   | <kbd>gf</kbd> etc.           | `ffap`[^1] and co.                           |
| Find and replace         | <kbd>:s</kbd>                | <kbd>M-%</kbd>                               |
| Line Numbers             | `number`                     | `(setq display-line-numbers t)`              |
| Relative Line Numbers    | `relativenumber`             | `(setq display-line-numbers 'relative)`[^2]  |
| Themes                   | <kbd>:colorscheme</kbd>      | `customize-themes`                           |
| Go to definition[^4]     | <kbd>gd</kbd> etc.           | <kbd>M-.</kbd>[^3]                           |
| Find references          | <kbd>gr</kbd>                | <kbd>M-?</kbd>                               |
| IDE-style location lists | Quickfix                     | FlyMake (or FlyCheck) and `next-error`       |
| Compiler Integration     | `compiler` and `makeprg`[^5] | `compile-command`                            |
| File-local variables     | modeline                     | `add-file-local-variable`                    |
| Directory-local vars     | `'exrc'`[^6]                 | `add-dir-local-variable`                     |
| Folding                  | `foldmethod`                 | `outline`, `hideshow` & <kbd>C-x $</kbd>[^7] |
| `grep` integration       | <kbd>:grep</kbd>             | `grep`                                       |
| Native regex search      | <kbd>:vimgrep</kbd>          | `occur`                                      |
| Single command execution | <kbd>:!</kbd>                | <kbd>M-!</kbd>, <kbd>M-&</kbd>               |
| Integrated terminal      | <kbd>:terminal</kbd>         | `term`, `shell`, `eshell` (& `vterm`)        |
| Abbreviations            | `abbreviations`              | `abbrev-mode`                                |

[^1]: The Emacs Evil mode <kbd>gf</kbd> binding does simply use `find-file-at-point`.

[^2]: Or `(setq display-line-numbers 'visual)`, which has subtle differences.

[^3]: `xref-find-definitions`

[^4]: Both this and ‘find references’ will use `ctags`/`etags` files, or just know what to do for symbols in the configuration language (VimL or Emacs Lisp).  These can easily be rebound or set up to work with LSP etc.

[^5]: The `makeprg` is run with the `:make` command.

[^6]: See <kbd>:h 'exrc'</kbd> and <kbd>:h trojan-horse</kbd>

[^7]: This is pretty much obsolete: use `hideshow` or `outline`.
