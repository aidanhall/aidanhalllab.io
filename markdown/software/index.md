# Writings on Software

I think about and mess around with software much more than I should.
Here are some posts about software that I have written over the years.

* [Software I Use](SSGPAGE(software.md)).
* [Thoughts on Vim](SSGPAGE(vim.md)).
* [Thoughts on Emacs](SSGPAGE(emacs.md)).
* [A breakdown of equivalent features in Vim and Emacs](SSGPAGE(emacs-vs-vim.md)).
