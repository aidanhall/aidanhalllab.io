# Personal Directory

This is the home of all my personal, non-tech-related content.

Prospective employers: **Go no further!**

## Blogs

* [Book Blog](SSGPAGE(../books/index.md)): What you would expect.
* [Media Blog](SSGPAGE(../media/index.md)): Manga, anime, video games
  etc.

## Unprofessional Web Presence

* [MyAnimeList](https://myanimelist.net/profile/Argletrough)
* [Neocities Site](https://argletrough.neocities.org)
* [YouTube](https://www.youtube.com/channel/UCrHVpobHpgGSBgZ9nExebaA)
