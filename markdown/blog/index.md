# Blog
This is my primary blog, concerning programs and technology I've
created or used, as well as more theoretical computer science topics.

[Feed](atom.xml).

## Posts
* 20/12/24:
  * 11:18: [New Emacs Configurations](SSGPAGE(24-12-20_11.18-New-Emacs-Configurations.md))
* 08/12/24:
  * 10:27: [AoC in C++ Data Structures and Algorithms: Day 8](SSGPAGE(24-12-08_10.27-AoC-in-C-Data-Structures-and-Algorithms-Day-8.md))
* 07/12/24:
  * 21:57: [AoC in C++ Algorithms: Day 7](SSGPAGE(24-12-07_21.57-AoC-in-C-Algorithms-Day-7.md))
* 06/12/24:
  * 11:10: [AoC in C++ Algorithms: Day 6](SSGPAGE(24-12-06_11.10-AoC-in-C-Algorithms-Day-6.md))
* 05/12/24:
  * 20:40: [AoC in C++ Algorithms: Day 5](SSGPAGE(24-12-05_20.40-AoC-in-C-Algorithms-Day-5.md))
  * 20:26: [AoC in C++ Algorithms: Day 4](SSGPAGE(24-12-05_20.26-AoC-in-C-Algorithms-Day-4.md))
  * 20:20: [AoC in C++ Algorithms: Day 3](SSGPAGE(24-12-05_20.20-AoC-in-C-Algorithms-Day-3.md))
  * 19:55: [AoC in C++ Algorithms: Day 2](SSGPAGE(24-12-05_19.55-AoC-in-C-Algorithms-Day-2.md))
  * 19:41: [AoC in C++ Algorithms: Day 1](SSGPAGE(24-12-05_19.41-AoC-in-C-Algorithms-Day-1.md))
* 14/11/24:
  * 20:43: [C++ Macros for Terse Lambda Expressions](SSGPAGE(24-11-14_20.43-C-Macros-for-Terse-Lambda-Expressions.md))
* 04/11/24:
  * 11:29: [Tracking Job Applications with Org Mode](SSGPAGE(24-11-04_11.29-Tracking-Job-Applications-with-Org-Mode.md))
* 01/08/24:
  * 19:52: [Eve of the Evolution Revolution](SSGPAGE(24-08-01_19.52-Eve-of-the-Evolution-Revolution.md))
* 19/06/24:
  * 09:15: [Setting Environment Variables In Emacs: PAGER and GTK_THEME](SSGPAGE(24-06-19_09.15-Setting-Environment-Variables-In-Emacs-PAGER-and-GTKTHEME.md))
* 03/05/24:
  * 13:27: [Emacs Desktop Environment: EXWM and Guix](SSGPAGE(24-05-03_13.27-Emacs-Desktop-Environment-EXWM-and-Guix.md))
* 20/03/24:
  * 20:47: [Minecraft with the Steam Controller](SSGPAGE(24-03-20_20.47-Minecraft-with-the-Steam-Controller.md))
* 25/08/23:
  * 18:43: [Seeing the Light of God-Mode](SSGPAGE(23-08-25_18.43-Seeing-the-Light-of-GodMode.md))
* 05/08/23:
  * 19:17: [All I'd Want to Blog in HTML](SSGPAGE(23-08-05_19.17-All-Id-Want-to-Blog-in-HTML.md))
* 11/06/23:
  * 12:35: [Semantic Newlines in Plain Text Typesetting](SSGPAGE(23-06-11_12.35-Semantic-Newlines-in-Plain-Text-Typesetting.md))
* 04/06/23:
  * 10:08: [New Wi-Fi Chip for my ThinkPad](SSGPAGE(23-06-04_10.08-New-WiFi-Chip-for-my-ThinkPad.md))
* 23/05/23:
  * 13:37: [Emacs: The Plan to Move Away from Evil Mode](SSGPAGE(23-05-23_13.37-Emacs-The-Plan-to-Move-Away-from-Evil-Mode.md))
* 03/05/23:
  * 11:47: [Adding What I Want (Arch Linux cleanup)](SSGPAGE(23-05-03_11.47-Adding-What-I-Want-Arch-Linux-cleanup.md))
* 21/04/23:
  * 12:35: [Emacs Lisp Scripting](SSGPAGE(23-04-21_12.35-Emacs-Lisp-Scripting.md))
* 19/02/23:
  * 13:37: [Using Template and Macro Dark Arts to Make a Smart C++ OpenGL Uniform Setter](SSGPAGE(23-02-19_13.37-Using-Template-and-Macro-Dark-Arts-to-Make-a-Smart-C-OpenGL-Uniform-setter.md))
* 27/01/23:
  * 17:03: [PlantUML: The LATEX of UML](SSGPAGE(23-01-27_17.03-PlantUML-The-LaTeX-of-UML.md))
* 31/12/22:
  * 17:22: [C++ Variadic Templates](SSGPAGE(22-12-31_17.22-C-Variadic-Templates.md))
* 30/10/22:
  * 10:34: [Thunderbird Supremacy](SSGPAGE(22-10-30_10.34-Thunderbird-Supremacy.md))
* 24/10/22:
  * 20:40: [Dogfooding Text Editors](SSGPAGE(22-10-24_20.40-Dogfooding-Text-Editors.md))
* 23/10/22:
  * 18:32: [CV Writing in LaTeX](SSGPAGE(22-10-23_18.32-CV-Writing-in-LaTeX.md))
* 16/10/22:
  * 15:10: [Thoughts on Meta-Programming](SSGPAGE(22-10-16_15.10-Thoughts-on-MetaProgramming.md))
* 22/09/22:
  * 17:46: [Guaranteeing Invariants on Classes with Private Constructors and friend](SSGPAGE(22-09-22_17.46-Guaranteeing-Invariants-on-Classes-with-Private-Constructors-and-friend.md))
* 16/09/22:
  * 20:47: [std::chrono Feels Like Magic](SSGPAGE(22-09-16_20.47-stdchrono-Feels-Like-Magic.md))
* 11/09/22:
  * 17:59: [C++ Lambdas Saving My Eyes](SSGPAGE(22-09-11_17.59-C-Lambdas-Saving-My-Eyes.md))
* 09/09/22:
  * 10:49: [What Modern C++ Nerds want you to Believe](SSGPAGE(22-09-09_10.49-What-Modern-C-Nerds-want-you-to-Believe.md))
* 16/08/22:
  * 01:11: [I Got Emacs Pinky](SSGPAGE(22-08-16_01.11-I-Got-Emacs-Pinky.md))
* 25/07/22:
  * 18:36: [Multiple Blog Directories](SSGPAGE(22-07-25_18.36-Multiple-Blog-Directories.md))
* 20/07/22:
  * 15:41: [Experiences from Writing a BrainFuck Compiler](SSGPAGE(22-07-20_15.41-Experiences-from-Writing-a-BrainFuck-Compiler.md))
* 14/06/22:
  * 11:36: [ælpa: Aidan's Emacs Lisp Package Archive](SSGPAGE(22-06-14_11.36-aelpa-Aidans-Emacs-Lisp-Package-Archive.md))
* 05/06/22:
  * 19:03: [Hïghly strenuous!!! $ßlog Þost ℕame?!](SSGPAGE(22-06-05_19.03-Highly-strenuous-sslog-THost-Name.md))
  * 08:21: [The Music Player Dæmon and its Clients](SSGPAGE(22-06-05_08.21-the-music-player-daemon-and-its-clients.md))
* 04/06/22:
  * 11:43: [Wired Headphones](SSGPAGE(22-06-04_11.43.md))
* 01/06/22:
  * 19:02: [Footnotes in markup and Emacs](SSGPAGE(22-06-01_19.02.md))
* 27/05/22:
  * 19:25: [Emoji](SSGPAGE(22-05-27_19.25.md))
* 17/05/22:
  * 21:46: [An Emacs script to define repeat maps.](SSGPAGE(22-05-17_21.46.md))
* 16/05/22:
  * 14:13: [Emacs GUI Decorations](SSGPAGE(22-05-16_14.13.md))
  * 12:44: [Centi-Blogging](SSGPAGE(22-05-16_12.44.md))
* 23/12/21:
  * 15:50: [Sticky Keys!!!](SSGPAGE(21-12-23_15.50.md))
* 31/10/21:
  * 18:09: [Diminish Emacs Minor Modes](SSGPAGE(21-10-31_18.09.md))
* 29/10/21:
  * 23:48: [Levels of Vim Emulation](SSGPAGE(21-10-29_23.48.md))
* 10/08/21:
  * 11:32: [I Believe in Trackball Supremacy](SSGPAGE(21-08-10_11.32.md))
* 21/07/21:
  * 10:19: [Emacs: I Broke](SSGPAGE(21-07-21_10.19.md))
* 08/07/21:
  * 22:11: [The Difference Between Vim and Emacs](SSGPAGE(21-07-08_22.11.md))
  * 10:29: [Quick Test](SSGPAGE(21-07-08_10.29.md))
* 06/07/21:
  * 10:35: [Justified Text: All the Cool Kids should use it!](SSGPAGE(21-07-06_10.35.md))
* 03/06/21:
  * 19:56: [Managing Vim Plugins with Git and Packages](SSGPAGE(21-06-03_19.56.md))
