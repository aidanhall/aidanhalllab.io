<article class="blogpost">
# Hïghly strenuous!!! $ßlog Þost ℕame?!
<div class="blogdate">Sun  5 Jun 19:03:44 BST 2022</div>

After the æ character in the filename of my previous blog post caused
it to simply not be loaded, I decided to do proper string
transliteration with uconv:

```shell
uconv -x Latin-ASCII
```

Applying that to the title of this post produced its file name,
which looks as weird you would expect.
I also removed any punctuation from the post name as well, since this
isn't entirely necessary for the filename to be meaningful.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
