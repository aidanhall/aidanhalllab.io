<article class="blogpost">
# What Modern C++ Nerds want you to Believe
<div class="blogdate">Fri  9 Sep 10:49:44 BST 2022</div>

Virgin procedural programming:

```cpp
  std::array<uint32_t, 5> high_scores;
  // ...
  std::string high_scores_string = "High Scores: ";
  for (const auto score : high_scores) {
    high_scores_string = high_scores_string + ", " + std::to_string(score);
  }
```

Chad functional programming:

```cpp
  std::array<uint32_t, 5> high_scores;
  // ...
  auto comma_fold = [](const std::string &text, uint32_t score) {
    return text + ", " + std::to_string(score);
  };
  std::string high_scores_string =
      std::accumulate(std::next(high_scores.begin()), high_scores.end(),
                      "High Scores: " + std::to_string(high_scores.front()), comma_fold);
```

I actually just wrote these two exact pieces of code, then looked
back, and questioned my life choices.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
