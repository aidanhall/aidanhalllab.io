<article class="blogpost">
# The Difference Between Vim and Emacs
<div class="blogdate">Thu  8 Jul 22:11:42 BST 2021</div>

I've used Vim for several years and have recently been getting into Emacs.
Now that I have an at least rudimentary understanding of both,
I can begin to articulate why it is so ridiculous to compare them
as if they are equivalent in purpose, capabilities or philosophy.

Vim is a text editor, and a pretty good one at that.
Emacs is a highly programmable development platform with a textual interface.
What's really important is the different approaches that
self-diagnosed 'Emacs users' and 'Vim users' take to using computers.

Vim is a virus that spreads through the user's computer,
infecting their other programs with
<kbd>h</kbd>
<kbd>j</kbd>
<kbd>k</kbd>
<kbd>l</kbd>
key bindings.
The limited scope of the Vim program
inspires its users to re-create a Vim-like experience
elsewhere,
pushing them towards
programs that run in the terminal with curses,
and use hot-keys and shortcuts instead of menus.

Emacs is the intergalactic hive-mind,
moving through the computer world
and assimilating all other programs
until the user's operating system is merely a
boot-loader for Emacs.
Text editor,
e-Mail client,
web browser,
calendar,
calculator,
even the X11 graphical session itself with EXWM;
if something can be implemented in Lisp,
it will become part of Emacs.
Do not resist.

In my opinion, when choosing between them,
the best mindset to take is that of _The Griller._
If you <q>just want to edit text for goodness sake</q>,
then the speed and simplicity of Vim
will be more appropriate.
However, when working on larger projects,
the more advanced feature-set
and extensibility of Emacs may be required.
However, I think it is important to keep boundaries
and treat Emacs as just an abnormally customisable IDE,
or else it may consume your life.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
