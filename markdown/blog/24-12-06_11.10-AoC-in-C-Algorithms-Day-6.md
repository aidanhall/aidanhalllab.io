<article class="blogpost">
# AoC in C++ Algorithms: Day 6
<div class="blogdate">Fri  6 Dec 11:10:55 GMT 2024</div>

I didn't manage to use any clever algorithms today, and only came up
with a slow, brute-force solution for part 2.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
