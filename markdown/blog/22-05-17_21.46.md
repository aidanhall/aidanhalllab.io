<article class="blogpost">
# An Emacs script to define repeat maps.
<div class="blogdate">Tue 17 May 21:46:21 BST 2022</div>

With lengthy key-chords and prefixes, a common desire among Emacs
users is being able to access a set of related keybindings in quick
succession, or just the same one repeatedly.

A long-standing solution for repeating has been the `repeat` function,
accessible via <kbd>C-x z</kbd>.
However, more robust solutions for accessing collections of related
bindings exist, including
[Hydra](https://github.com/abo-abo/hydra).
The built-in `transient` package can also be used for repeated/grouped
key-bindings, according to
[this Reddit thread](https://www.reddit.com/r/emacs/comments/mx6xs2/transient_vs_hydra/).

However, in Emacs 28, we got access to `repeat-mode`, which provides
built-in standardised support for repeating a lot of common
key-bindings, including window resizing and error list navigation.

I investigated this and came up with a tiny ELisp script to add
repeating keys based on `repeat-mode`:
[repeat-map-define.el](https://gitlab.com/aidanhall/repeat-map-define).

This was also, excitingly my first ‘proper’ stand-alone ELisp script.
Once you've loaded it, you can add keybindings like this:

```lisp
(repeat-map-define
 defun-repeat-map
 '(
   ("a" beginning-of-defun)
   ("e" end-of-defun)
   ))
```

Corresponding to the `global-map` feature of Hydra, we can bind a key
to the map itself to get access to the bindings with a prefix:

```lisp
(global-set-key (kbd "C-x c") defun-repeat-map)
```

Note the lack of a quote before the reference to `defun-repeat-map`,
since the map only exists as a variable and not a function.
If a function is desired, the following somewhat ugly workaround can
be used. I got this from the definition of `vc-prefix-map`.

```lisp
(fset 'defun-repeat-map defun-repeat-map)
```

I'm pleased with the results of this, and feel like it aligns well
with my notion of ‘vanilla Emacs’,
which I discussed in [my page about Emacs](SSGPAGE(../emacs.md)).

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
