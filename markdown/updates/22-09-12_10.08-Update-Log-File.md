<article class="blogpost">
# Update Log File
<div class="blogdate">Mon 12 Sep 10:08:13 BST 2022</div>

I finally got round to fixing the configuration for my
[update log file](SITE_URL/changes.html).
It contains a list of every page on the site, sorted by most recent
commit time.

I've put in a `grep` statement to hopefully filter out unnecessary noise
updates like those for the blog indices, since these will only be
updated when a new post is added, which is what should appear at the
top in that case.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
