# Site Updates

These will relate to SSGSS, as well as the structure and content.

[Feed](atom.xml).

## Posts
* 21/03/24:
  * 01:10: [Atom Feeds](SSGPAGE(24-03-21_01.10-Atom-Feeds.md))
* 11/06/23:
  * 12:17: [New Homepage Layout](SSGPAGE(23-06-11_12.17-New-Homepage-Layout.md))
* 20/10/22:
  * 13:47: [Grouped Post](SSGPAGE(22-10-20_13.47-Grouped-Post.md))
  * 13:45: [Fancy Blog Logs](SSGPAGE(22-10-20_13.45-Fancy-Blog-Logs.md))
* 16/10/22:
  * 12:17: [Rolling Logs in All Blogs](SSGPAGE(22-10-16_12.17-Rolling-Logs-in-All-Blogs.md))
* 12/09/22:
  * 10:08: [Update Log File](SSGPAGE(22-09-12_10.08-Update-Log-File.md))
* 05/06/22:
  * 19:38: [Moving to Gitlab Pages](SSGPAGE(22-06-05_19.38-Moving-to-Gitlab-Pages.md))
* 04/06/22:
  * 13:24: [Testing Blog Script Improvements!](SSGPAGE(22-06-04_13.24-testing-blog-script-improvements!.md))
* 04/07/21:
  * 17:35: [Web Page Benchmarking](SSGPAGE(21-07-04_17.35.md))
  * 17:12: [Final Fixing](SSGPAGE(21-07-04_17.12.md))
  * 16:26: [Blog Advancements](SSGPAGE(21-07-04_16.26.md))
  * 16:24: [Testing The Advancement](SSGPAGE(21-07-04_16.24.md))
  * 14:07: [Article Tags](SSGPAGE(21-07-04_14.07.md))
* 24/05/21:
  * 14:03: [Procrastination](SSGPAGE(21-05-24_14.03.md))
  * 13:57: [The Blog](SSGPAGE(21-05-24_13.57.md))
