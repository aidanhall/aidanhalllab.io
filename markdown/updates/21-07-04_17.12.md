<article class="blogpost">
# Final Fixing
After yet another few hours of hell, it's finally done.
I've decided that this blog will probably never exceed a few hundred posts
so people's computers should be able to handle large files.

I am now just `cat`ing all of the markdown files for the blog
into one large file which is used to produce the rolling log.
I know it is less efficient and elegant, but I just want the pain to end.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
