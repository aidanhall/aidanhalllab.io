<article class="blogpost">
# Fancy Blog Logs
<div class="blogdate">Thu 20 Oct 13:45:15 BST 2022</div>

You may have noticed the new, fancy format for the post logs on my
blogs.
The existing entries were rearranged with a whacking great sed (really
`:s`) command,
and the blog generation script has (hopefully) been updated to use
this format too.

This includes functionality to group together posts made on the same
day, which aren't uncommon.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
