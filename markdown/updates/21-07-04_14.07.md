<article class="blogpost">
# Article Tags
My blog posts are now surrounded by `<article>` tags.
This will make it possible (and very easy) to have a rolling view of
my entire blog by con*cat*enating all of the markdown files.

</article>
