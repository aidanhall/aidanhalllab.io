<article class="blogpost">
# Rolling Logs in All Blogs
<div class="blogdate">Sun 16 Oct 12:17:59 BST 2022</div>

Going back on my statement in
[the post where I announced support for multiple blog directories](
SSGPAGE(22-07-25_18.36-Multiple-Blog-Directories.md)),
I have added support for having rolling blog views (with all the posts
made on a blog in a single file) in all my blog directories.
The solution is actually fairly powerful.

The main innovation was the use of a Make function.
Fiddling around with recipes let me set things up
so any file called `rolling.md` gets treated as a rolling blog file,
which includes every post in its directory,
and probably sub-directories.

```make
ROLLINGBLOGFILENAME = rolling.md
BLOGFILESIN = $(shell find $(1) -type f -name "*_*.md" | sort -rn)

$(MDROOTDIR)/%/$(ROLLINGBLOGFILENAME): $(ROLLINGBLOGTEMPLATE) $(MDFILES)
	cat $< $(call BLOGFILESIN, $(dir $@)) > $@
```

I now have three directories set up as blogs:

* [The tech blog](SITE_URL/blog/index.html) (this one),
* [the book blog](SITE_URL/books/index.html), and
* [the media blog](SITE_URL/media/index.html).

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
