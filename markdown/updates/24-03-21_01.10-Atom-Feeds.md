<article class="blogpost">
# Atom Feeds
<div class="blogdate">Thu 21 Mar 01:10:10 GMT 2024</div>

After a few too many years, I finally hacked together a script to
generate mostly valid Atom 1.0 feeds for my blogs. Check the "Feed"
link at the top of each blog index page, and in the metadata of every
page in a blog directory. For now, they just have the headlines and
dates, so consumers will have to visit the website to read the posts.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
