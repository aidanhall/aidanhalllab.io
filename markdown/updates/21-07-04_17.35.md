<article class="blogpost">
# Web Page Benchmarking

There are many ways to assess the quality of a website,

* aesthetics,
* loading times,
* content,
* or even the kinds of people who use it.

However, I have found the only truly accurate method for discerning whether
a website is worth your time:

<blockquote><b>How well can it can be viewed on a Kindle?</b></blockquote>

FLOATFIG(This website being rendered by the kindle., ../pics/kindle.webp, )

## Backstory
Amazon created an 'Experimental Browser' for the Kindle many years ago.
It was, frankly speaking, terrible, due in no small part to the technical limitations
imposed by the e-ink screen.
In their unending wisdom, rather than improving it, they decided to keep the
'experimental' name, allowing them to totally excuse the fact that it
completely fails to render anything more than _the simplest of websites._

## Method
However, we can view this as a blessing in disguise:
Through the lens of the Kindle browser we can see the modern web for what it truly is,
a bloated, over-complicated mess of memory- and processor-intensive technologies
that will bring all but the most recent (and marketable) computers to their knees.
If a website can be successfully viewed on the Kindle,
its creator has clearly considered the importance and elegance of simplicity.

## Criteria
There are some fairly obvious criteria for a website to work well on a Kindle:

* No animations.
* A simple layout.
* **Highly limited** use of JavaScript and images.
* Black text on a white background (preferable).

Based on this, we may present some examples:

* [The first website.](http://info.cern.ch/)
* [Bjarne Strostrup's website](https://stroustrup.com), as well as those of a perhaps unsurprising number of other programming language creators.
* [This page](http://xahlee.info), which I found somewhere down the Emacs rabbit hole.

## Closing Thoughts
Default HTML is [the perfect form of the web](https://www.motherfuckingwebsite.com/),
and we should try to get [as close as is reasonable to it](https://bestmotherfucking.website/).

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
