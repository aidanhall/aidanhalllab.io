# Book Blog

Occasional reviews or thoughts on books I've read.

[Feed](atom.xml).

## Posts
* 08/08/23:
  * 08:21: [Fermat's Last Theorem](SSGPAGE(23-08-08_08.21-Fermats-Last-Theorem.md))
* 16/08/22:
  * 01:53: [Guards! Guards!](SSGPAGE(22-08-16_01.53-Guards-Guards.md))
