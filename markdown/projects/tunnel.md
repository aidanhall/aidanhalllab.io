# Tunnel Game

FLOATFIG(Gameplay Screenshot, SITE_URL/pics/tunnel-screenshot.png)

This is a game I made for the Global Game Jam 2022.
[Demo video.](https://youtu.be/Gn7uLat1oqQ "A short demo of me playing through the game.")

Wave-particle duality is the concept that for quantum-scale objects, their
behaviours can't all be explained by modelling them as a particle or a wave.
The player controls a cannon that can fire ‘particles’ (photons), with the goal
of being absorbed by an electron.

The photon can only be absorbed as a wave, so the player must switch it to
acting as one.

However, there are barriers in the way that will absorb the wave if it passes
all the way through, or reflect the particle, lowering its speed.
The particle can only bounce 6 times before it is destroyed.
The player can switch the wave back to a particle by observing its position
while it is part-way through a barrier, using quantum tunnelling to go through.
The outside of the electron also acts as a barrier.

The game supports keyboard and gamepad.
Nintendo button labels have been used.

The player can adjust the initial speed of the particle, which can increase or decrease the challenge to suit.

## Links

* [Sources (GitLab)](https://gitlab.com/aidanhall/tunnel).
* [Itch Page (Downloads)](https://aidan-hall.itch.io/tunnel).
* [Global Game Jam Page](https://globalgamejam.org/2022/games/tunnel-3).
