# Software Portfolio

## Games

* [Itch.io](https://aidan-hall.itch.io/): I will upload new games
  there instead of adding pages for them here.
* [My A-Level Project, a 2D platformer](SSGPAGE(a-level-project.md)).
* [A C++ and SDL Space Invaders clone](https://github.com/aidan-hall/space-invaders-sdl).
* [A C and SDL Tetris clone](https://github.com/aidan-hall/tetris-sdl/).
* [Tunnel, a puzzle game](SSGPAGE(tunnel.md)).
<!-- * [Dungeon Quest](SITE_URL/docs/dungeon.py), a text adventure I wrote in year 7. -->

## Game Engine Tools and Systems

* [Tilemap Editor](https://gitlab.com/aidanhall/doge-tilemap-editor).
* [Entity Component System](https://gitlab.com/aidanhall/tecs).

## Other Programs

* [A BrainFuck Compiler](SSGPAGE(bf-compiler.md)).
* [A Chip-8 Virtual Machine](https://github.com/aidan-hall/chip8-rust).
