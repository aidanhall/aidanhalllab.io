# A-Level Project

FLOATFIG(A-Level Programming Project Screenshot,SITE_URL/pics/a-level-project.webp,)


I made this game for my Computer Science A-Level.
The source code is [here](https://gitlab.com/aidanhall/a-level-programming-project-version-3).

Amusingly, though perhaps not surprisingly, this video is
less than half the file size of the write-up:

<figure><video controls src="SITE_URL/pics/a-level-project-demo.mp4">A short demo.</video></figure>

