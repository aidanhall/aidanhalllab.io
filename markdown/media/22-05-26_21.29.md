<article class="blogpost">
# Endless Eight: An Experience
<div class="blogdate">Thu 26 May 21:29:53 BST 2022</div>

I've been watching Haruhi (2006 broadcast order, naturally), and am
now moving on to the 2009 batch.  *Maple Leaf Rhapsody* was good, and
I just finished watching *Endless Eight*.  I'll be interested to see
where the series goes from here!

(For your amusement, I will be role-playing as someone unaware of what
happens next.)

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
