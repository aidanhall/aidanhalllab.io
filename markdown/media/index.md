# Media

Things such as anime, films and video games... but mostly anime.
I have included some context for certain sets of posts.

[Feed](atom.xml).

## Posts
* 11/03/23:
  * 10:40: [Jojo's Bizarre Adventure Part 8](SSGPAGE(23-03-11_10.40-Jojos-Bizarre-Adventure-Part-8.md))
* 31/12/22:
  * 17:56: [One Piece: The Rest of the Chapters (551-1071)](SSGPAGE(22-12-31_17.56-One-Piece-The-Rest-of-the-Chapters-5511071.md))
* 04/11/22:
  * 20:58: [One Piece: The First 550 Chapters](SSGPAGE(22-11-04_20.58-One-Piece-The-First-550-Chapters.md))
* 16/10/22:
  * 12:29: [AKIRA](SSGPAGE(22-10-16_12.29-AKIRA.md))
* 28/05/22:
  * 21:59: [Endless Eight: An Escape?](SSGPAGE(22-05-28_21.59.md))
  * 18:10: [Endless Eight: An Immersion](SSGPAGE(22-05-28_18.10.md))
  * 17:42: [Endless Eight: A Resignation](SSGPAGE(22-05-28_17.42.md))
* 27/05/22:
  * 22:05: [Endless Eight: A Motif](SSGPAGE(22-05-27_22.05.md))
  * 19:17: [Endless Eight: An Annoyance](SSGPAGE(22-05-27_19.17.md))
* 26/05/22:
  * 21:58: [Endless Eight: An Analysis](SSGPAGE(22-05-26_21.58.md))
  * 21:29: [Endless Eight: An Experience](SSGPAGE(22-05-26_21.29.md))
