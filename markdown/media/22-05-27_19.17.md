<article class="blogpost">
# Endless Eight: An Annoyance
<div class="blogdate">Fri 27 May 19:17:58 BST 2022</div>

Ok, let's drop the act.
Episode 2 was at least differentiated from episode 1 by the fact that
they weren't aware of the loop the first time, if I recall.
By contrast, episode 3 was pretty much the same as episode 2, with
those minor differences that Nagato loves to rattle off.

That being said, I think I'll keep going a little longer.
The issue is that the thing to do will probably be to skip to the last
episode once I've gotten bored, but then I'll still have one
more episode to watch *after* the point of boredom.
I can only imagine the pain of experiencing this weekly, and slowly
realising they really were going to do it for eight episodes.

<!-- Leave a blank line after the last paragraph of the post! -->
</article>
