# Writings

* [An essay about the problems with plain-text code (originally titled: ‘Why Are We Stuck with FORTRAN?’)](SSGPAGE(typewritten-code.md)) (12/05/2022).
* [EPQ: ‘Creating a Robot that Models Asimov's Three Laws of Robotics’](
SSGPAGE(epq.md)) (19/02/2021).
