# Why does English-based ‘typewritten’ code remain dominant, and what problems does it present?
## Or, Why Are We Stuck with FORTRAN?

<span class="blogdate">12/05/2022</span>

[PDF](SITE_URL/docs/typewritten-code.pdf)

## Abstract

All popular, modern programming languages are based on plain, ASCII
text with English identifiers.  The style is archaic, making code
harder to read, especially for non-native English speakers.  It
developed within the technical limitations of typewriters, but has
persisted due to language designers only making improvements
incrementally, and dependence on existing tools and libraries.  In
this article, I propose ways to improve programming languages,
including Unicode operators and identifiers based on non-English
natural languages.  I also discuss the virtues of programming
paradigms other than imperative; I consider functional and symbolic
(for their reduced use of English identifiers), as well as literate
programming, which shifts the focus of programming away from computer
execution towards communication between humans.  I then discuss the
potential practice of using multiple notations in unison, in a form of
‘heterogeneous programming’.  None of the ideas I discuss can see
quick adoption, but can be gradually implemented over time as new
languages and developments arise, with the ultimate goals of making
programming more accessible and expressive.
