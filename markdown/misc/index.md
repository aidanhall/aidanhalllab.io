# Miscellaneous Pages

* [A Rant About Equations](SSGPAGE(equations.md)).
* [Educational Resources I Like](SSGPAGE(educational.md)).
* [(Computer Science) Quotes](SSGPAGE(quotes.md)).

