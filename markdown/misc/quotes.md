# Quotes

Here is a collection of interesting quotes related to computer science
that I have found over the years.

<hr/>

<figure>
<blockquote>
Emacs causes you to think about ways to write programs to do editing
for you rather than ways to write programs to write programs for
you...
It treats editing as an end rather than a means.
Editing itself is not an interesting process;
it is what we edit that is interesting.
</blockquote>
<figcaption>Doug Hoyte, <cite>Let Over Lambda</cite>.</figcaption>
<details>
<summary>Analysis</summary>
<p>
The relevance of this quote can be realised quite easily:
There are many Emacs packages such as `abbrev`, `yasnippet`,
`tempo` and `skeleton` whose primary purposes are to automate the
typing of commonly-occurring textual structures.
Hoyte's implication seems to be that, if it's possible and worthwhile
to define a snippet (or template, or whatever) for a code form,
it makes more sense to define an abstraction (such as a Lisp macro)
within the language of the code you are writing.
This relates strongly to
[my blog post about meta-programming](SITE_URL/blog/22-10-16_15.10-Thoughts-on-MetaProgramming.html).
</p></details>
</figure>


<hr/>

<figure>
<blockquote>
Threads are *evil*,
because what's the difference between a thread and a process?
Threads *can* fuck up each other's internal data structures.
</blockquote>
<figcaption>
Joe Armstrong,
<cite><a href="https://youtu.be/cNICGEwmXLU?t=2920">Systems that run forever self-heal and scale</a></cite>.
</figcaption>
</figure>

<hr/>

<figure>
<blockquote>
A user can think they understand what they are doing, but they're
really just copy-and-pasting code around. Programming thus becomes
akin to magical rituals: you put certain bits of code before other
bits, and everything seems to work.
</blockquote>
<figcaption>
Jason L. McKesson,
<cite>
<a href="https://paroj.github.io/gltut/About%20this%20Book.html#idp148">
Learn Modern 3D Graphics Programming</a></cite>.
</figcaption>
</figure>

<hr/>

<figure>
<blockquote>
Get a job
<br/>
Sha na na na na na na na na
<br/>
Get a job
<br/>
Sha na na na na na na na na
<br/>
Wah yip yip yip yip yip yip yip yip yip
<br/>
Sha boom
</blockquote>
<figcaption>Harold Ableson and Gerald Jay Sussman,
<cite>Structure and Interpretation of Computer Programs</cite>.</figcaption>
</figure>

<hr/>

<figure>
<blockquote>
Modern languages are so poorly designed that they hinder you more than
they help when you are trying to actually program at a high level if
you care about what the results are at the low level, which I do.
</blockquote>
<figcaption>Casey Muratori,
<cite><a href="https://hero.handmade.network/forums/code-discussion/t/51-how_do_you_do_metaprogramming_in_c">Handmade Hero Forums</a></cite>.</figcaption>
</figure>

<hr/>

<figure>
<blockquote>
The idea that anyone would ever actually do manual memory management
in C these days was just unthinkable—I mean, after all, it’s *current
year*, and everyone knows that once it’s *current year*, an arbitrary
set of ideas that I don’t like become intrinsically false.
</blockquote>
<figcaption>Ryan Fleury,
<cite><a href="https://www.rfleury.com/p/untangling-lifetimes-the-arena-allocator">Untangling Lifetimes: The Arena Allocator</a></cite>.
</figcaption>
</figure>

<hr/>

<figure>
<blockquote>
Disclaimer: This guide will appear vague and incomplete if you
aren't sure what you're doing. This is intentional. This is
specifically not designed for users new to software compilation
and toolchain components.
</blockquote>
<figcaption>Arch Linux ARM,
<cite><a href="https://archlinuxarm.org/wiki/Distcc_Cross-Compiling">Distcc Cross-Compiling</a></cite>.
</figcaption>
</figure>
<!-- fig>bq+figcaption>cite -->
