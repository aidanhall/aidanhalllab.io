# Formatting Equations on the WWW
There are various technologies that _could_ be used for creating equations on the web.
The rendering tools below are supported by Pandoc,
allowing the insertion of equations using TeX syntax.

## Image Files
The simplest (backend-wise) option is just storing the equation as an image.
This can work, if you don't mind having your website look like
[something from the '90s.](http://hyperphysics.phy-astr.gsu.edu/hbase/mot.html#motcon)
It's also annoying since you have to manually format the equation,
export it to an image file,
then insert with an `<img>` tag.

## [MathML](https://www.w3.org/1998/Math/MathML/)
What _should_ be the standard is MathML.
It's probably faster than image files,
since the code can be embedded in the HTML file,
and doesn't require JavaScript.

However,
despite a web standard, it is only supported by FireFox and Safari.
As an additional kick in the teeth, its rendering is regrettably inferior to other options.

## [MathJax](https://www.mathjax.org/)
This is what Google wants us to use, so it's slow and full of JavaScript.

## [Webtex](https://pkgw.github.io/webtex/)
This is another option supported by Pandoc, but has one up on MathJax
by working automatically.
On the downside, it's slower and looks rubbish.

## Unicode
If you're feeling adventurous, you can use Unicode characters for some basic equations.
Here is an example.

<div class="maths">∫ x dx = ½x² + c</div>

## Conclusion
It pains me to admit that MathJax is objectively the best in terms of quality,
and is usually fast enough.
The script will also render MathML in the page, as well as TeX commands.

However, MathML works without JavaScript so I'll be using it here
until I am able (and want) to host my own non-static site.

If this doesn't render properly, get a better browser:

<div class="maths">$\int x dx = \frac{1}{2} x^2 + c$</div>
