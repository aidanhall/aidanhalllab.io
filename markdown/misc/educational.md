# Educational Resources I Like
These are some (predominantly web-based)
educational resources that I have found useful.
They are in no particular order.

## [The Arch Linux Wiki](https://wiki.archlinux.org/)
This has lots of information about software configuration,
and includes a functional installation guide
for Arch Linux.
What more can you ask for?
Despite its name,
almost any page that doesn't concern package management
or OS installation
will be applicable to most Linux distributions.
Wiki sites are a good stepping stone for moving away from using forums
for technical problems.

## [MDN Web Docs](https://developer.mozilla.org/)
This is an indispensable resource for core web development technologies.
It has more technical information than I can comprehend
and concrete examples for using HTML tags and CSS styles.
The simple styling and greater level of detail
make it vastly superior to w3schools
as a reference.

## [Making Pictures With GNU PIC](http://floppsie.comp.glam.ac.uk/Glamorgan/gaius/web/pic.html)
This one is mostly here for historical reasons,
since I've had a link to it on my home page for the life of this site.
I've found GNU's documentation for Groff preprocessors somewhat
insufficient.
While `man pic` is better than most,
this site was useful for more detailed explanations when I was actively using
pic.

## [Cprogramming.com](https://www.cprogramming.com/)
This site has no-nonsense tutorial introductions to the basics
of C and C++.
I used it to get started with these languages.
There is other content on there which I haven't looked at,
but might be a bit outdated and overly Windows-oriented for my tastes.

## [Learn OpenGL](https://learnopengl.com/)
A freely available online version of an entire _book_
about using OpenGL.
There is also a [PDF version](https://learnopengl.com/book/book_pdf.pdf)
available.
As of writing, I've worked through the 'Getting Started' section,
which guides you through creating a 3D scene and a camera
that can move within it.

The examples use C++ and the GLM maths library,
but it either contains or links to sufficient explanations
of the concepts and maths involved
to allow a competent and patient reader to use alternatives.
I've been using the Zig programming language,
and implementing the functionality used from GLM
myself.

## [Xah Lee's Website](https://xahlee.info)
An assortment of articles on
topics related to maths and programming,
two of my biggest interests.
I haven't spent too much time on here,
but the sheer volume and variety of content that
has apparently been produced by this one person
is admirable.

## [Lazy Foo' Productions](https://lazyfoo.net)
This website isn't so modern and chic as others, but the content is
great for people wanting to get into (engine-less) game development.
The SDL tutorial on there actually covers a lot more than just SDL,
encompassing a lot of the basics of (2D) game architecture, including
animation, collisions and tiling.
