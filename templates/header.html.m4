<!-- -*- mode: html; -*- -->
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>PAGETITLE - SITENAME</title>
m4_ifdef(`HEADCONTENTS', HEADCONTENTS, `')
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="STYLESHEET"/>
<link rel="apple-touch-icon" sizes="180x180" href="SITE_URL/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="SITE_URL/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="SITE_URL/favicon-16x16.png">
<link rel="manifest" href="SITE_URL/site.webmanifest">
m4_esyscmd(test -f PAGEDIR/atom.xml && echo '<link rel="alternate" type="application/atom+xml" title="Atom Feed" href="atom.xml">')
</head>
<body>
<main id="top">
