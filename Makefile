include config.mk

MDDIRS := $(shell find $(MDROOTDIR) -type d)

SITEDIRS := $(subst $(MDROOTDIR), $(SITEROOTDIR), $(MDDIRS))


vpath %.md $(MDDIRS)

MDFILESIN = $(shell find $(1) -type f -name "*.md")
MDFILES = $(call MDFILESIN, $(MDROOTDIR)) $(CHANGESFILE)
BLOGFILESIN = $(shell find $(1) -type f -name "*_*.md" | sort -rn)
BLOGFEEDS = $(patsubst %, $(SITEROOTDIR)/%/atom.xml, $(BLOGDIRS))
HTMLFILES := $(subst $(MDROOTDIR), $(SITEROOTDIR), $(patsubst %.md, %.html, $(MDFILES)))


# implicit(?) rules
$(SITEROOTDIR)/%.html : %.md $(HEADERFILE) $(FOOTERFILE)
	mkdir -p $(shell dirname $@)
	$(MARKDOWN) < $<\
		| $(M4)\
		-D PAGETITLE="$(shell awk '/^#/{ sub(/^# /, NULL); print; exit}' $< )" \
		-D DATE="$(shell date +'%d/%m/%y')" \
		-D COMMITDATE="$(shell git log --date=format:"%d/%m/%y" --pretty=format:'%cd, %h' -n 1 $<)" \
		-D PAGEDIR="$(shell dirname $@)" \
		$(M4FILES) $(HEADERFILE) - $(FOOTERFILE)\
		| sed '/^\s*$$/d' \
		> $@

$(SITEROOTDIR)/%/atom.xml : $(MDROOTDIR)/%/index.md
	mkdir -p $(shell dirname $@)
	./atom-feed.sh $< > $@


$(MDROOTDIR)/%/$(ROLLINGBLOGFILENAME): $(ROLLINGBLOGTEMPLATE) $(MDFILES)
	cat $< $(call BLOGFILESIN, $(dir $@)) > $@

# explicit rules

all: $(BLOGFEEDS) changes $(HTMLFILES) verbatim
	echo "blogfeeds:" $(BLOGFEEDS)
	echo $(MDROOTDIR)

$(HTMLFILES): $(M4FILES)


$(CHANGESFILE): $(CHANGESTEMPLATE) $(MDFILES)
	cat $< > $@
	sh ./recent.sh >> $@

changes: $(CHANGESFILE)

$(SITEROOTDIR):
	mkdir $@


verbatim: $(RAWDIR)
	cp -ruv $(RAWDIR)/* $(SITEROOTDIR)


clean:
	$(RM) -rv $(SITEROOTDIR)/*

force: clean all

upload: all
	rsync -avXuz --delete-after $(SITEROOTDIR)/\
		$(WEBSERVER)$(SERVERDIR)

view: all
	setsid xdg-open $(SITEROOTDIR)/index.html &
