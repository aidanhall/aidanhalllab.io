#!/bin/sh

# BLOGROLLING="$BLOGDIR/rolling.md"

printf "Blog Title: "
[ -n "$1" ] && BLOGTITLE="$1"
[ -z "$BLOGTITLE" ] && read -r BLOGTITLE

DEFAULT_BLOGDIR="markdown/blog"

printf "Blog Directory (default %s): " "$DEFAULT_BLOGDIR"
read -r BLOGDIR
[ -z "$BLOGDIR" ] && BLOGDIR="$DEFAULT_BLOGDIR"

mkdir -p "$BLOGDIR"

echo blogging "$BLOGTITLE"

BLOGNAME="$(date +'%y-%m-%d_%H.%M')-$(echo $BLOGTITLE | sed 's/[[:punct:]]//g;s/\s/-/g' | uconv -x Latin-ASCII )"
BLOGDATE="$(date +'%d/%m/%y')"
BLOGDATEESCAPED="$(date +'%d\/%m\/%y')"
BLOGTIME="$(date +'%H:%M')"
BLOGFILE="$BLOGNAME.md"
BLOGINDEX="$BLOGDIR/index.md"

sed -i "/^\* $BLOGDATEESCAPED:/d;/^## Posts/a * $BLOGDATE:\n  * $BLOGTIME: [$BLOGTITLE](SSGPAGE($BLOGFILE))" "$BLOGINDEX"
sed "s/BLOGTITLE/$BLOGTITLE/g; s/DATE/$(date)/g;" templates/blog.md > "$BLOGDIR/$BLOGFILE"

echo "Created blog file: $BLOGDIR/$BLOGFILE"
