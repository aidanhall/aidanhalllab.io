#!/bin/sh

getupdated() {
    git -P log --date=format:"20%y-%m-%dT%H:%M:%SZ" --pretty=format:'%cd' -n 1 $@
}

FNAME=$(realpath -s --relative-to="$PWD" "$1")

BASE=$(dirname $FNAME | sed 's/markdown//')

awk -v siteurl="https://aidanhall.gitlab.io" \
    -v base="$BASE" \
    -f atom-feed.awk \
     $FNAME
