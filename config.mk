# progs
M4 = m4 -P
MARKDOWN = pandoc -f markdown-smart -t html --mathml

# dirs
ROOT = $(PWD)

TEMPLATEDIR = $(ROOT)/templates

MDROOTDIR = $(ROOT)/markdown

SITEROOTDIR = $(ROOT)/public

RAWDIR = $(ROOT)/raw

BLOGDIRS = blog books media updates

# server
WEBSERVER = aidan@uwcs.co.uk
SERVERDIR = :~/public_html

# files

M4FILES  = $(ROOT)/util.m4 $(ROOT)/config.m4
HEADERFILE = $(TEMPLATEDIR)/header.html.m4
FOOTERFILE = $(TEMPLATEDIR)/footer.html.m4
ROLLINGBLOGTEMPLATE = $(TEMPLATEDIR)/rollingblog.md

ROLLINGBLOGFILENAME = rolling.md

CHANGESTEMPLATE = $(TEMPLATEDIR)/changes.md
CHANGESFILE = $(MDROOTDIR)/changes.md
