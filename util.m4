m4_define(`defblank', `m4_define($1, ``'')')
m4_define(`SSGPAGE', `m4_patsubst(`m4_patsubst($1, .md, .html)', markdown/, site/)')
m4_define(`SSGCODE', `m4_esyscmd(`source-highlight -s $1 -f html <<EOF
$2')')
