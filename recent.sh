#!/bin/sh
# Disclaimer: Some of the logic of this file was copied from a StackOverflow post,
# but I can't remember where.  Sorry!

echo "<ul>"
cd markdown || exit 1

for f in $(find ** -name "*.md" | grep -v "changes.md\|blog/index.md\|books/index.md\|rolling.md"); do
    git --no-pager log --color -1 --date=short \
	--pretty=format:"%ct <li><a href='SSGPAGE($f)'>%cs - $f</a></li>%n" \
	-- $f
done | sort -rn | sed 's/^[0-9]* //'


cd - > /dev/null
echo "</ul>"
